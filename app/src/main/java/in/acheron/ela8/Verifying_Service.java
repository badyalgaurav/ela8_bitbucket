package in.acheron.ela8;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.util.List;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import static android.content.ContentValues.TAG;

public class Verifying_Service extends Service {
    private Looper mServiceLooper;
    private Verifying_Service.ServiceHandler mServiceHandler;

    public static final int notify = 10*1000;  //interval between two services(Here Service run every 5 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling

    private LocationListener listener;
    private LocationManager locationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }
            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1);
        }
    }

    @Override
    public void onCreate() {

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new Verifying_Service.ServiceHandler(mServiceLooper);




    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
        else

            mTimer = new Timer();   //recreate new
        mTimer.scheduleAtFixedRate(new TimeDisplay(), 10*1000, notify);   //Schedule task

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();    //For Cancel Timer
        Toast.makeText(this, "Service Stops", Toast.LENGTH_SHORT).show();

    }

    //API 21 and above
    public String getProcessNew() {
        String topPackageName="";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            // We get usage stats for the last 10 seconds
            List < UsageStats > stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
            // Sort the stats by the last time used
            if (stats != null) {
                SortedMap < Long, UsageStats > mySortedMap = new TreeMap < Long, UsageStats > ();
                for (UsageStats usageStats: stats) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    topPackageName = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
//                    Toast.makeText(Verifying_Service.this, topPackageName, Toast.LENGTH_SHORT).show();
//                                Log.e("TopPackage Name", topPackageName);
                }
            }
        }
        return topPackageName;
    }

    //API below 21
    @SuppressWarnings("deprecation")
    private String getProcessOld(){
        String topPackageName = null;
        ActivityManager activity = (ActivityManager) Verifying_Service.this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTask = activity.getRunningTasks(1);
        if (runningTask != null) {
            ActivityManager.RunningTaskInfo taskTop = runningTask.get(0);
            ComponentName componentTop = taskTop.topActivity;
            topPackageName = componentTop.getPackageName();
        }
        return topPackageName;
    }




    //class TimeDisplay for handling task
    class TimeDisplay extends TimerTask {
        @Override
        public void run() {

            // run on another thread
            mHandler.post(new Runnable() {
                @SuppressLint("MissingPermission")
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void run() {

                    final Intent i = new Intent("Details");

                    ///Foreground TEST
                    String appName;
                    if (Build.VERSION.SDK_INT >= 21) {
                        appName= getProcessNew();

                    } else {
                        appName= getProcessOld();
                    }


                    listener = new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {

//                            Intent i = new Intent("Details");
                            i.putExtra("coordinates",location.getLatitude()+","+location.getLongitude());

//                            sendBroadcast(i);
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {

                        }

                        @Override
                        public void onProviderEnabled(String provider) {

                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            Intent i= new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }

                    };
                    // getting GPS status
                    locationManager =(LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
                    //noinspection ResourceType
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, listener);


                    i.putExtra("appName",appName);
                    sendBroadcast(i);
//                    Toast.makeText(Verifying_Service.this, appName, Toast.LENGTH_SHORT).show();



// WORKING EXAMPLE
//                    String topPackageName;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
//                        long time = System.currentTimeMillis();
//                        // We get usage stats for the last 10 seconds
//                        List < UsageStats > stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);
//                        // Sort the stats by the last time used
//                        if (stats != null) {
//                            SortedMap < Long, UsageStats > mySortedMap = new TreeMap < Long, UsageStats > ();
//                            for (UsageStats usageStats: stats) {
//                                mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
//                            }
//                            if (mySortedMap != null && !mySortedMap.isEmpty()) {
//                                topPackageName = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
//                                Toast.makeText(Verifying_Service.this, topPackageName, Toast.LENGTH_SHORT).show();
////                                Log.e("TopPackage Name", topPackageName);
//                            }
//                        }
//                    }


                }
            });
        }
    }

    public void GetLatLong()
    {

    }
}
