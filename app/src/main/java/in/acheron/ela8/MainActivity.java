package in.acheron.ela8;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private Button btnOn,btnOff;
    String imeistring, appDetails;
    private TextView txtView;
    private BroadcastReceiver broadcastReceiver;
    //Root URL of our web service
    public static final String ROOT_URL = "http://appdatareceiver.acheron.in/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //THIS IS INITILIZING AREA
        btnOn=(Button) findViewById(R.id.btnOn);
        btnOff=(Button) findViewById(R.id.btnOff);
        txtView = (TextView) findViewById(R.id.textView);

        if (!runtime_permission()) {
            // enable_buttons();
        }
    }
    private  boolean runtime_permission()
    {
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},100);
            return  true;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 100)
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // enable_buttons();
            } else {
                runtime_permission();
            }
    }


    public void startService(View view) {
       //this is used to get the access permissioin from user for app access
        Intent intent_access_Settings = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        startActivity(intent_access_Settings);



        //this is for accessing the service (Verifying_Service)
        Intent i = new Intent(getApplicationContext(),Verifying_Service.class);
        startService(i);
    }
    // Method to stop the service
    public void stopService(View view) {
        Intent i = new Intent(getApplicationContext(),Verifying_Service.class);
        stopService(i);
        // stopService(new Intent(getBaseContext(), MyService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        //for getting Unique number
        /*
        getDeviceId() function Returns the unique device ID.
        */

        imeistring = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        // Register for the particular broadcast based on ACTION string
        if(broadcastReceiver == null)
        {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String  mobileDetails=String.valueOf(intent.getExtras().get("appName"));
                    String coordinates=String.valueOf(intent.getExtras().get("coordinates"));
                    if(coordinates == "null")
                    {
                        coordinates="0,0";
                    }
                    float batteryStatus= getBatteryLevel();
                    //TO CHECK WHETHER PHONE IS LOCKED OR NOT
                    KeyguardManager keyguardManager = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
                    if( keyguardManager.inKeyguardRestrictedInputMode()) {
                        //it is locked

                        appDetails=mobileDetails+','+String.valueOf(batteryStatus)+','+imeistring+','+getDateTime()+','+1+','+coordinates;
                        apicall(appDetails);
                        //phone was unlocked, do stuff here
                        txtView.setText(String.valueOf(("phone  locked"+coordinates)));
                    } else {
                        //it is not locked
                        appDetails=mobileDetails+','+String.valueOf(batteryStatus)+','+imeistring+','+getDateTime()+','+0+','+coordinates;
                        apicall(appDetails);

                    }
                 //End Of TO CHECK WHETHER PHONE IS LOCKED OR NOT


                }
            };

        }
        registerReceiver(broadcastReceiver, new IntentFilter("Details"));
    }

    public void apicall(String appDetails)
    {
        //WEB API CALLING PORTION

        //use for creating multiple parameters
        Map<String, String> params = new HashMap<String, String>();
        params.put("mobileDetail", appDetails);

        //Creating a rest adapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();

        //Creating an object of our api interface
        IWebAPI api = adapter.create(IWebAPI.class);

        //Defining the method
        api.getDetails(params,new Callback<CallBackAPIHelper>() {
            @Override
            public void success(CallBackAPIHelper list, Response response) {
                // Toast.makeText(ButtonActivity.this, "Student Record Deleted", Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError error) {
                //you can handle the errors here
                //Toast.makeText(ButtonActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra("resultCode", RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                String resultValue = intent.getStringExtra("resultValue");
                Toast.makeText(MainActivity.this, resultValue, Toast.LENGTH_SHORT).show();
            }
        }
    };

    //    for Battery Percentage
    public float getBatteryLevel() {
        Intent batteryIntent = registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        // Error checking that probably isn't needed but I added just in case.
        if(level == -1 || scale == -1) {
            return 50.0f;
        }

        return ((float)level / (float)scale) * 100.0f;
    }
    //for getting datetime
    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

}
