package in.acheron.ela8;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.QueryMap;



import java.util.List;
        import java.util.Map;

        import retrofit.Callback;
        import retrofit.http.Body;
        import retrofit.http.DELETE;
        import retrofit.http.GET;
        import retrofit.http.POST;
        import retrofit.http.PUT;
        import retrofit.http.Path;
        import retrofit.http.QueryMap;

public interface IWebAPI {
    //Retrofit turns our institute WEB API into a Java interface.
    //So these are the list available in our WEB API and the methods look straight forward



    //i.e. http://localhost/api/institute/Students/1
    //Get student record base on ID


    @GET("/TaskManagement/ToCompleteTask")

    public void ToCompleteTask(@QueryMap Map<String, String> params, Callback<String> callback);



    @GET("/OrderTracking/ToActivateOrderTracking")

    public void ToActivateOrderTracking(@QueryMap Map<String, String> params, Callback<String> callback);



    @GET("/Receiver/InsertAppData")

    public void getDetails(@QueryMap Map<String, String> params,Callback<CallBackAPIHelper> callback);

    //i.e. http://localhost/api/institute/Students/1
    //Delete student record base on ID
    @DELETE("GPSLatLongAPI/InsertLat?lat={lat}")
    public void deleteStudentById(@QueryMap Map<String, String> params, Callback<CallBackAPIHelper> callback);

    //i.e. http://localhost/api/institute/Students/1
    //PUT student record and post content in HTTP request BODY
    @PUT("/institute/Students/{id}")
    public void updateStudentById(@Path("id") Integer id, @Body CallBackAPIHelper student, Callback<CallBackAPIHelper> callback);

    //i.e. http://localhost/api/institute/Students
    //Add student record and post content in HTTP request BODY
    @POST("/institute/Students")
    public void addStudent(@Body CallBackAPIHelper student,Callback<CallBackAPIHelper> callback);

}